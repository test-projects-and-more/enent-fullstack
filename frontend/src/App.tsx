import React from "react";
import {
  Navigate,
  Route,
  BrowserRouter as Router,
  Routes,
} from "react-router-dom";
import "./App.css";
import CityList from "./pages/city-list";
import EventDetails from "./pages/event-details";
import EventList from "./pages/events-list";
import { routes } from "./utils/constants/routes";

function App() {
  return (
    <Router>
      <Routes>
        <Route
          path={routes.home}
          element={
            <div className="App">
              <CityList />
            </div>
          }
        />
        <Route
          path={routes.city}
          element={
            <div className="App">
              <EventList />
            </div>
          }
        />
        <Route
          path={routes.event}
          element={
            <div className="App">
              <EventDetails />
            </div>
          }
        />
        <Route path={routes.event} element={<div className="App">Event</div>} />
        <Route path="*" element={<Navigate to={routes.home} replace />} />
      </Routes>
    </Router>
  );
}

export default App;
