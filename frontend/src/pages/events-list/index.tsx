import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import Pagination from "../../components/pagination";
import { fetchAllEvents } from "../../features/events/slices";
import { getEventPC, getEvents } from "../../features/events/slices/selectors";
import { AppDispatch } from "../../redux/store";
import { commonText, eventsText } from "../../utils/constants/dictionary";
import { getFormattedDate } from "../../utils/helpers/get-formatted-date";
import "./styles.css";

const EventList: React.FC = () => {
  const { id } = useParams();
  const dispatch = useDispatch<AppDispatch>();

  const events = useSelector(getEvents);
  const [category, setCategory] = useState<string>("");
  const [startDate, setStartDate] = useState<Date | null>(null);
  const [endDate, setEndDate] = useState<Date | null>(null);
  const [tags, setTags] = useState<string>("");

  const pagesCount = useSelector(getEventPC);
  const [currentPage, setCurrentPage] = useState<number>(1);

  const handlePageChange = (page: number) => {
    setCurrentPage(page + 1);
  };

  useEffect(() => {
    dispatch(
      fetchAllEvents({
        filters: { category, endDate, startDate, tags, cityId: Number(id) },
        pageNumber: currentPage,
      })
    );
  }, [id, category, startDate, endDate, tags, currentPage]);

  return (
    <div>
      <h1>Events in City</h1>
      <div>
        <label>
          {eventsText.categoryTitle}:
          <input
            type="text"
            value={category}
            onChange={(e) => setCategory(e.target.value)}
          />
        </label>
      </div>
      <div>
        <label>
          {eventsText.startDateTitle}:
          <input
            type="date"
            value={startDate ? startDate.toISOString().split("T")[0] : ""}
            onChange={(e) => setStartDate(new Date(e.target.value))}
          />
        </label>
      </div>
      <div>
        {eventsText.endDateTitle}:
        <input
          type="date"
          value={endDate ? endDate.toISOString().split("T")[0] : ""}
          onChange={(e) => setEndDate(new Date(e.target.value))}
        />
      </div>
      <div>
        <label>
          {eventsText.tagsTitle}:
          <input
            type="text"
            value={tags}
            onChange={(e) => setTags(e.target.value)}
          />
        </label>
      </div>

      <div>
        {events.map(({ title, id, category, date, web_tag_groups }) => (
          <Link key={id} to={`/${commonText.event}/${id}`}>
            <div className="event" key={id}>
              <h4>{title}</h4>
              <p>
                {eventsText.categoryTitle}: {category}
              </p>
              <p>
                {eventsText.dateTitle}: {getFormattedDate(new Date(date))}
              </p>
              <p>
                {eventsText.tagsTitle}: {web_tag_groups}
              </p>
            </div>
          </Link>
        ))}
      </div>

      <Pagination
        pagesCount={pagesCount}
        pageNumber={currentPage}
        onChange={handlePageChange}
      />
    </div>
  );
};

export default EventList;
