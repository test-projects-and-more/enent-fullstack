import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Pagination from "../../components/pagination";
import { ICity } from "../../features/cities/models/city";
import { fetchAllCities } from "../../features/cities/slices";
import { getCities, getCitiesPC } from "../../features/cities/slices/selectors";
import { AppDispatch } from "../../redux/store";
import { commonText } from "../../utils/constants/dictionary";
import "./styles.css";

const CityList: React.FC = () => {
  const dispatch = useDispatch<AppDispatch>();
  const cities = useSelector(getCities);
  const pagesCount = useSelector(getCitiesPC);
  const [currentPage, setCurrentPage] = useState<number>(1);

  useEffect(() => {
    dispatch(fetchAllCities(currentPage));
  }, [dispatch, currentPage]);

  const handlePageChange = (page: number) => {
    setCurrentPage(page + 1);
  };

  return (
    <div>
      <h1>City List</h1>
      <ul className="cities-list">
        {cities &&
          cities.map((city: ICity) => (
            <Link key={city.id} to={`/${commonText.city}/${city.id}`}>
              <li>{city.name}</li>
            </Link>
          ))}
      </ul>

      <Pagination
        pagesCount={pagesCount}
        pageNumber={currentPage}
        onChange={handlePageChange}
      />
    </div>
  );
};

export default CityList;
