import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { fetchEventInfo } from "../../features/events/slices";
import { getEvent } from "../../features/events/slices/selectors";
import { AppDispatch } from "../../redux/store";
import { commonText, eventsText } from "../../utils/constants/dictionary";
import { getFormattedDate } from "../../utils/helpers/get-formatted-date";

const EventDetails: React.FC = () => {
  const { id } = useParams();
  const dispatch = useDispatch<AppDispatch>();
  const event = useSelector(getEvent);

  const eventDate = event.date ? getFormattedDate(new Date(event.date)) : "";

  useEffect(() => {
    if (id) {
      dispatch(fetchEventInfo(Number(id)));
    }
  }, [id]);

  if (!event.id) {
    return <div>{commonText.loading}</div>;
  }

  return (
    <div>
      <h1>{event.title}</h1>
      <div dangerouslySetInnerHTML={{ __html: event.description }} />
      <p>
        {eventsText.dateTitle}: {eventDate}
      </p>
      <p>
        {eventsText.categoryTitle}: {event.category}
      </p>
      <p>
        {eventsText.venueNameTitle}: {event.venue.name}
      </p>
      <p>
        {eventsText.venueAddressTitle}: {event.venue.address}
      </p>
    </div>
  );
};

export default EventDetails;
