import { combineReducers, configureStore } from '@reduxjs/toolkit';
import citiesSlice from '../features/cities/slices/index';
import eventsSlice from '../features/events/slices/index';

const rootReducer = combineReducers({
  cities: citiesSlice,
  events: eventsSlice,
});

export const store = configureStore({
  reducer: rootReducer,
});

export type AppDispatch = typeof store.dispatch;
