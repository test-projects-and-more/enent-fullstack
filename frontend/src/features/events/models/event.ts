export interface IEventsStore {
  events: IEvent[];
  event: IEvent;
  filters: IEventsFilters;
  offset: number;
  pagesCount: number;
}

export interface IEvent {
  id: number;
  title: string;
  description: string;
  date: string;
  image: string;
  url: string;
  category: string;
  age: number;
  web_tag_groups: string;
  dateType: string;
  minPrice: number;
  maxPrice: number;
  cityId: number;
  venue: IVenue;
}

export interface IEventsFilters {
  cityId?: number;
  category?: string;
  startDate?: Date | null;
  endDate?: Date | null;
  tags?: string;
}

export interface IVenue {
  id: number;
  name: string;
  address: string;
  alias: string;
  googleAddress: string;
}
