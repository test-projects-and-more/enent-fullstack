import { IStore } from "../../../models/store";

export const getEvents = (state: IStore) => state.events.events;
export const getEvent = (state: IStore) => state.events.event;
export const getEventPC = (state: IStore) => state.events.pagesCount;

export const getEventsFilters = (state: IStore) => state.events.filters;
