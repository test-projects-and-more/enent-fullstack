import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { getAllEventsPath, getEventInfoPath } from "../../../api/events";
import { defaultLimit, startPage } from "../../../utils/constants/cities";
import { actionsText } from "../../../utils/constants/dictionary";
import { defaultEventsFilters } from "../../../utils/constants/events";
import { getEventsFilters } from "../../../utils/helpers/get-events-filters";
import {
  getPaginationOffset,
  getPaginationPagesCount,
} from "../../../utils/helpers/get-pagination-info";
import { IEvent, IEventsFilters, IEventsStore } from "../models/event";

interface IFetchAllEventsProps {
  filters: IEventsFilters;
  pageNumber: number;
}

export const fetchAllEvents = createAsyncThunk(
  `${actionsText.events.name}/${actionsText.events.fetchAllEvents}`,
  async (
    { filters, pageNumber = startPage }: IFetchAllEventsProps,
    { dispatch }
  ) => {
    dispatch(clearEventsFilters());

    const offset = getPaginationOffset(pageNumber);

    const { data } = await axios.get(
      `${getAllEventsPath}?${actionsText.limit}=${defaultLimit}&${
        actionsText.offset
      }=${offset}&${getEventsFilters(filters)}`
    );

    dispatch(updateEventsPagesCount(getPaginationPagesCount(data.count)));

    return data.data;
  }
);

export const fetchEventInfo = createAsyncThunk(
  `${actionsText.events.name}/${actionsText.events.fetchEventInfo}`,
  async (eventId: number) => {
    const response = await axios.get(`${getEventInfoPath}/${eventId}`);

    return response.data;
  }
);

const initialState: IEventsStore = {
  events: [],
  event: {} as IEvent,
  filters: defaultEventsFilters,
  offset: 0,
  pagesCount: 0,
};

const eventsSlice = createSlice({
  name: actionsText.events.name,
  initialState,
  reducers: {
    updateEventsPagesCount(state: IEventsStore, action: PayloadAction<number>) {
      state.pagesCount = action.payload;
    },
    updateEventsStartDateFilter(
      state: IEventsStore,
      action: PayloadAction<Date>
    ) {
      state.filters.startDate = action.payload;
    },
    updateEventsEndDateFilter(
      state: IEventsStore,
      action: PayloadAction<Date>
    ) {
      state.filters.endDate = action.payload;
    },
    updateEventsCategoryFilter(
      state: IEventsStore,
      action: PayloadAction<string>
    ) {
      state.filters.category = action.payload;
    },
    updateEventsTagsFilter(state: IEventsStore, action: PayloadAction<string>) {
      state.filters.tags = action.payload;
    },
    clearEventsFilters(state: IEventsStore) {
      state.filters = defaultEventsFilters;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      fetchAllEvents.fulfilled,
      (state: IEventsStore, action: PayloadAction<IEvent[]>) => {
        state.events = action.payload;
      }
    );
    builder.addCase(
      fetchEventInfo.fulfilled,
      (state: IEventsStore, action: PayloadAction<IEvent>) => {
        state.event = action.payload;
      }
    );
  },
});

export const {
  updateEventsPagesCount,
  updateEventsStartDateFilter,
  updateEventsEndDateFilter,
  updateEventsCategoryFilter,
  updateEventsTagsFilter,
  clearEventsFilters,
} = eventsSlice.actions;

export default eventsSlice.reducer;
