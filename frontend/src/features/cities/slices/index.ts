import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { getAllCitiesPath, getCityInfoPath } from "../../../api/cities";
import {
  defaultCityInfo,
  defaultLimit,
  startPage,
} from "../../../utils/constants/cities";
import { actionsText } from "../../../utils/constants/dictionary";
import {
  getPaginationOffset,
  getPaginationPagesCount,
} from "../../../utils/helpers/get-pagination-info";
import { ICitiesStore, ICity } from "../models/city";

export const fetchAllCities = createAsyncThunk(
  `${actionsText.cities.name}/${actionsText.cities.fetchAllCities}`,
  async (pageNumber: number = startPage, { dispatch }) => {
    const offset = getPaginationOffset(pageNumber);

    const { data } = await axios.get(
      `${getAllCitiesPath}?${actionsText.limit}=${defaultLimit}&${actionsText.offset}=${offset}`
    );

    dispatch(updateCitiesPagesCount(getPaginationPagesCount(data.count)));

    return data.data;
  }
);

export const fetchCityInfo = createAsyncThunk(
  `${actionsText.cities.name}/${actionsText.cities.fetchCityInfo}`,
  async (cityId: number) => {
    const response = await axios.get(`${getCityInfoPath}/${cityId}`);

    return response.data;
  }
);

const initialState: ICitiesStore = {
  cities: [],
  city: defaultCityInfo,
  offset: 0,
  pagesCount: 0,
};

const citiesSlice = createSlice({
  name: actionsText.cities.name,
  initialState,
  reducers: {
    updateCitiesPagesCount(state: ICitiesStore, action: PayloadAction<number>) {
      state.pagesCount = action.payload;
    },
    updateCitiesPaginationOffset(
      state: ICitiesStore,
      action: PayloadAction<number>
    ) {
      state.offset = getPaginationOffset(action.payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(
      fetchAllCities.fulfilled,
      (state: ICitiesStore, action: PayloadAction<ICity[]>) => {
        state.cities = action.payload;
      }
    );
    builder.addCase(
      fetchCityInfo.fulfilled,
      (state: ICitiesStore, action: PayloadAction<ICity>) => {
        state.city = action.payload;
      }
    );
  },
});

export const { updateCitiesPagesCount, updateCitiesPaginationOffset } =
  citiesSlice.actions;

export default citiesSlice.reducer;
