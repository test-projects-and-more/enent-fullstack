import { IStore } from "../../../models/store";

export const getCities = (state: IStore) => state.cities.cities;
export const getCitiesPC = (state: IStore) => state.cities.pagesCount;

export const getCity = (state: IStore) => state.cities.city;
