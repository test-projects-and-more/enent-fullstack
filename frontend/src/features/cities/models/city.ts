export interface ICitiesStore {
  cities: ICity[];
  city: ICity;
  offset: number;
  pagesCount: number;
};

export interface ICity {
  id: number;
  name: string;
};