const base = "http://127.0.0.1:3000";
const event = "event";

export const getAllEventsPath = `${base}/${event}`;
export const getEventInfoPath = `${base}/${event}`;
