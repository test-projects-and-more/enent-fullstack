const base = "http://127.0.0.1:3000";
const city = "city";

export const getAllCitiesPath = `${base}/${city}`;
export const getCityInfoPath = `${base}/${city}`;
