import { ICity } from "../../features/cities/models/city";

export const defaultCityInfo: ICity = {
  id: 0,
  name: '',
};

export const startPage = 1;

export const defaultLimit = 20;