export const routes = {
  home: "/",
  city: "/city/:id",
  event: "/event/:id",
};
