import { IEventsFilters } from "../../features/events/models/event";

export const defaultEventsFilters: IEventsFilters = {
  startDate: null,
  endDate: null,
  category: "",
  tags: "",
};
