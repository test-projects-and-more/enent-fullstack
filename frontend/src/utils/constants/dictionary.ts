export const commonText = {
  pagination: {
    next: "next >",
    previous: "< previous",
    breakLabel: "...",
  },
  loading: "Loading...",
  city: "city",
  event: "event",
};

export const actionsText = {
  limit: "limit",
  offset: "offset",
  cities: {
    name: "cities",
    fetchAllCities: "fetchAllCities",
    fetchCityInfo: "fetchCityInfo",
  },
  events: {
    name: "events",
    fetchAllEvents: "fetchAllEvents",
    fetchEventInfo: "fetchEventInfo",
  },
  venue: {
    name: "venue",
    fetchVenueInfo: "fetchVenueInfo",
  },
};

export const eventsText = {
  startDate: "startDate",
  endDate: "endDate",
  category: "category",
  tag: "tags",
  cityId: "cityId",
  dateTitle: "Date",
  tagsTitle: "Tags",
  categoryTitle: "Category",
  startDateTitle: "Start Date",
  endDateTitle: "End Date",
  venueNameTitle: "Venue name",
  venueAddressTitle: "Venue Address",
};
