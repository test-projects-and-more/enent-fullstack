import { IEventsFilters } from "../../features/events/models/event";
import { eventsText } from "../constants/dictionary";

export const getEventsFilters = ({
  startDate,
  endDate,
  category,
  tags,
  cityId,
}: IEventsFilters) => {
  const filters: string[] = [];

  if (startDate) {
    filters.push(`${eventsText.startDate}=${startDate}`);
  }

  if (endDate) {
    filters.push(`${eventsText.endDate}=${endDate}`);
  }

  if (category) {
    filters.push(`${eventsText.category}=${category}`);
  }

  if (tags) {
    filters.push(`${eventsText.tag}=${tags}`);
  }

  if (cityId) {
    filters.push(`${eventsText.cityId}=${cityId}`);
  }

  return filters.length ? filters.join("&") : "";
};
