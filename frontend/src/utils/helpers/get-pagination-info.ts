import { defaultLimit, startPage } from "../constants/cities";

export const getPaginationOffset = (pageNumber: number = startPage): number => {
  return (pageNumber - 1) * defaultLimit;
};

export const getPaginationPagesCount = (count: number): number => {
  return Math.ceil(count / defaultLimit);
};
