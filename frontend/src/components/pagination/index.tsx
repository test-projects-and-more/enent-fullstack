import React, { FC } from "react";
import ReactPaginate from "react-paginate";
import { commonText } from "../../utils/constants/dictionary";
import "./styles.css";

interface IPaginationProps {
  pageNumber?: number;
  pagesCount: number;
  onChange?: any;
}

const Pagination: FC<IPaginationProps> = ({
  pageNumber,
  pagesCount,
  onChange,
}) => {
  return (
    <div id="container">
      <ReactPaginate
        breakLabel={commonText.pagination.breakLabel}
        nextLabel={commonText.pagination.next}
        previousLabel={commonText.pagination.previous}
        pageCount={pagesCount}
        pageRangeDisplayed={5}
        onPageChange={({ selected }) => onChange(selected)}
        pageClassName="page-item"
        pageLinkClassName="page-link"
        previousClassName="page-item"
        previousLinkClassName="page-link"
        nextClassName="page-item"
        nextLinkClassName="page-link"
        breakClassName="page-item"
        breakLinkClassName="page-link"
        containerClassName="pagination"
        activeClassName="active"
        renderOnZeroPageCount={null}
      />
    </div>
  );
};

export default Pagination;
