import { ICitiesStore } from "../features/cities/models/city";
import { IEventsStore } from "../features/events/models/event";

export interface IStore {
  cities: ICitiesStore;
  events: IEventsStore;
}
