import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CityModule } from './modules/city/city.module';
import { EventModule } from './modules/event/event.module';
import { VenueModule } from './modules/venue/venue.module';
import { TypeOrmConfigService } from './typeorm/typeorm.service';

@Module({
  imports: [
    EventModule,
    CityModule,
    VenueModule,
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      useClass: TypeOrmConfigService,
      imports: undefined,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
