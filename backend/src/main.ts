import { NestFactory } from '@nestjs/core';
import * as cors from 'cors';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(
    cors({
      credentials: true,
      origin: ['http://localhost:3001'],
    }),
  );
  const port = process.env.PORT;
  await app.listen(port, () => {
    console.log('[WEB]', port);
  });
}
bootstrap();
