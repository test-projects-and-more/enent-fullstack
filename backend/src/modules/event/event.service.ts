import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  Between,
  ILike,
  LessThanOrEqual,
  MoreThanOrEqual,
  Repository,
} from 'typeorm';
import { Event } from './event.entity';

@Injectable()
export class EventService {
  @InjectRepository(Event)
  private readonly repository: Repository<Event>;

  async getAll(filters: {
    cityId?: string;
    category?: string;
    startDate?: string;
    endDate?: string;
    tags?: string;
    limit: number;
    offset: number;
  }): Promise<{ data: Event[]; count: number }> {
    const { limit, offset, tags } = filters;

    const where: any = {};

    if (filters.cityId) {
      where.city_id = filters.cityId;
    }

    if (filters.category) {
      where.category = ILike(`%${filters.category}%`);
    }

    if (filters.startDate && filters.endDate) {
      const startDate = new Date(filters.startDate);
      const endDate = new Date(filters.endDate);
      where.date = Between(startDate, endDate);
    } else if (filters.startDate) {
      const startDate = new Date(filters.startDate);

      where.date = MoreThanOrEqual(startDate);
    } else if (filters.endDate) {
      const endDate = new Date(filters.endDate);
      where.date = LessThanOrEqual(endDate);
    }

    if (tags) {
      where.web_tag_groups = ILike(`%${tags}%`);
    }

    const events = await this.repository.findAndCount({
      where,
      take: limit,
      skip: offset,
    });

    return { data: events[0], count: events[1] };
  }

  public getOne(id: number): Promise<Event> {
    return this.repository.findOne({ where: { id } });
  }
}
