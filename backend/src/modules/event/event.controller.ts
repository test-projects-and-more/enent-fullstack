import {
  Controller,
  DefaultValuePipe,
  Get,
  Inject,
  Param,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { Event } from './event.entity';
import { EventService } from './event.service';

@Controller('event')
export class EventController {
  @Inject(EventService)
  private readonly service: EventService;

  @Get()
  async getAll(
    @Query()
    filters: {
      cityId?: string;
      category?: string;
      startDate?: string;
      endDate?: string;
      tags?: string;
    },
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
    @Query('offset', new DefaultValuePipe(0), ParseIntPipe) offset: number,
  ): Promise<{ data: Event[]; count: number }> {
    return this.service.getAll({
      ...filters,
      limit,
      offset,
    });
  }

  @Get(':id')
  async getOne(@Param('id') id: number): Promise<Event> {
    return this.service.getOne(id);
  }
}
