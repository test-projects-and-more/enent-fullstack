import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { City } from '../city/city.entity';
import { Venue } from '../venue/venue.entity';

@Entity()
export class Event {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ type: 'varchar' })
  public title: string;

  @Column({ type: 'varchar' })
  public description: string;

  @Column()
  public date: Date;

  @Column({ type: 'varchar' })
  public image: string;

  @Column({ type: 'varchar' })
  public url: string;

  @Column({ type: 'varchar' })
  public category: string;

  @Column({ type: 'integer' })
  public age: number;

  @Column({ type: 'varchar' })
  public web_tag_groups: string;

  @Column({ type: 'varchar' })
  public date_type: string;

  @Column({ type: 'integer' })
  public min_price: number;

  @Column({ type: 'integer' })
  public max_price: number;

  @Column({ type: 'integer' })
  public city_id: number;

  @Column({ type: 'integer' })
  public venue_id: number;

  @ManyToOne(() => City, { eager: true })
  @JoinColumn({ name: 'city_id' })
  city: City;

  @ManyToOne(() => Venue, { eager: true })
  @JoinColumn({ name: 'venue_id' })
  venue: Venue;
}
