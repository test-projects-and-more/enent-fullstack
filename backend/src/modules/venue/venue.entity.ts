import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Venue {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ type: 'varchar' })
  public name: string;

  @Column({ type: 'varchar' })
  public address: string;

  @Column({ type: 'varchar' })
  public alias: string;

  @Column({ type: 'varchar' })
  public google_address: string;
}
