import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Venue } from './venue.entity';

@Injectable()
export class VenueService {
  @InjectRepository(Venue)
  private readonly repository: Repository<Venue>;

  public getAll() {
    return this.repository.find();
  }

  public getOne(id: number): Promise<Venue> {
    return this.repository.findOne({ where: { id } });
  }
}
