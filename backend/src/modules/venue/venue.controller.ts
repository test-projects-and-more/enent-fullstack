import { Controller, Get, Inject } from '@nestjs/common';
import { VenueService } from './venue.service';

@Controller('venue')
export class VenueController {
  @Inject(VenueService)
  private readonly service: VenueService;

  @Get()
  async findAll() {
    return await this.service.getAll();
  }
}
