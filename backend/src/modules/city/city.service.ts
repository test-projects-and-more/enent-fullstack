import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { City } from './city.entity';

@Injectable()
export class CityService {
  @InjectRepository(City)
  private readonly repository: Repository<City>;

  public async getAll(
    limit: number,
    offset: number,
  ): Promise<{ data: City[]; count: number }> {
    const cities = await this.repository.findAndCount({
      take: limit,
      skip: offset,
      order: { name: 'ASC' },
    });

    return { data: cities[0], count: cities[1] };
  }

  public getOne(id: number): Promise<City> {
    return this.repository.findOne({ where: { id } });
  }
}
