import {
  Controller,
  DefaultValuePipe,
  Get,
  Inject,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { City } from './city.entity';
import { CityService } from './city.service';

@Controller('city')
export class CityController {
  @Inject(CityService)
  private readonly service: CityService;

  @Get()
  async getAll(
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
    @Query('offset', new DefaultValuePipe(0), ParseIntPipe) offset: number,
  ): Promise<{ data: City[]; count: number }> {
    return this.service.getAll(limit, offset);
  }
}
