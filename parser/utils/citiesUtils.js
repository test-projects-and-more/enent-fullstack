const { getUniqueValues } = require("./getUniqueValues");

function getCitiesData(data) {
  const cities = getUniqueValues(data.map((item) => item.region[0]));
  return cities.map((city, index) => {
    return {
      id: index + 1,
      name: city,
    };
  });
}

module.exports = {
  getCitiesData,
};
