function getUniqueValues(arr) {
  return Array.from(new Set(arr));
}

module.exports = {
  getUniqueValues,
};
