function getEventsData(data, cities) {
  return data.map((item) => {
    const city_id =
      cities.findIndex((city) => city.name === item.region[0]) + 1;
    return {
      id: item.id[0],
      title: item.title[0],
      description: item.description[0],
      date: new Date(item.date[0]),
      image: item.image[0],
      url: item.url[0],
      category: item.category[0],
      age: item.age[0],
      web_tag_groups: item.web_tag_groups[0],
      date_type: item.date_type[0],
      min_price: item.min_price[0],
      max_price: item.max_price[0],
      city_id: city_id,
      venue_id: item.venue_id[0],
    };
  });
}

module.exports = {
  getEventsData,
};
