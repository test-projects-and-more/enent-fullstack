const { getUniqueValues } = require("./getUniqueValues");

function getVenuesData(data) {
  const venues = getUniqueValues(data.map((item) => item.venue_id[0]));
  return venues.map((venue, index) => {
    const venueData = data.find((item) => item.venue_id[0] === venue);
    return {
      id: venueData.venue_id[0],
      name: venueData.venue[0],
      address: venueData.venue_address[0],
      alias: venueData.venue_alias[0],
      google_address: venueData.google_address[0],
    };
  });
}

module.exports = {
  getVenuesData,
};
