const fs = require("fs");
const { parseString } = require("xml2js");
const db = require("./db");
const { getCitiesData, getVenuesData, getEventsData } = require("./utils");

const xmlFile = fs.readFileSync("export.xml", "utf-8");

parseString(xmlFile, (err, result) => {
  if (err) {
    console.error("Ошибка при разборе XML:", err);
    return;
  }
  const data = result.subevents.subevent;

  // Извлечение уникальных городов и площадок
  const cities = getCitiesData(data);
  const venues = getVenuesData(data);

  // Формирование данных для таблицы events
  const events = getEventsData(data, cities);

  async function insertData(table, data) {
    try {
      for (const item of data) {
        const columns = Object.keys(item).join(",");
        const values = Object.values(item).map((value) =>
          typeof value === "string" ? value : JSON.stringify(value)
        );

        const placeholders = values
          .map((_, index) => `$${index + 1}`)
          .join(", ");
        const query = {
          text: `INSERT INTO ${table} (${columns}) VALUES (${placeholders})`,
          values: values,
        };

        await db.query(query.text, query.values);
      }

      console.log(`Данные успешно загружены в таблицу ${table}`);
    } catch (error) {
      console.error(`Ошибка при загрузке данных в таблицу ${table}:`, error);
    }
  }

  // Загрузка данных в таблицы
  async function loadData() {
    try {
      await insertData("city", cities);
      await insertData("venue", venues);
      await insertData("event", events);

      console.log("Все данные успешно загружены!");
      process.exit(0); // Завершение процесса Node.js
    } catch (error) {
      console.error("Ошибка при загрузке данных:", error);
      process.exit(1); // Завершение процесса Node.js с ошибкой
    }
  }

  loadData();
});
